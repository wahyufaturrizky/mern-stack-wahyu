let express = require("express");
let properties = require("./config/properties");
let db = require("./config/database");
let app = express();

// call the database connectivity function
db();

app.listen(properties.PORT, (req, res) => {
  console.log(`Server is running on ${properties.PORT} port.`);
});
